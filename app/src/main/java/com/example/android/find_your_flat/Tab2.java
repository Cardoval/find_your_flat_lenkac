package com.example.android.find_your_flat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by andronov on 21-May-17.
 */


/**
 * An activity that displays a Google map with a marker (pin) to indicate a particular location.
 */
public class Tab2 extends Fragment implements OnMapReadyCallback {

    GoogleMap mGoogleMap;
    MapView mMapView;
    View mView;
    public Tab2(){
        //required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        // Retrieve the content view that renders the map.
//        setContentView(R.layout.tab2);
//        // Get the SupportMapFragment and request notification
//        // when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.tab2, container, false);

        SupportMapFragment mapFragment=(SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Button button_center = (Button) mView.findViewById(R.id.button_center);

        button_center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getContext(),"Click Me", Toast.LENGTH_LONG);
                toast.show();
                SimpleOrientationActivity myOrientationEventListener = new SimpleOrientationActivity();
            }
        });

        return mView;
    }

//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        mMapView = (MapView) mMapView.findViewById(R.id.map);
//        if(mMapView!=null){
//            mMapView.onCreate(null);
//            mMapView.onResume();
//            mMapView.getMapAsync(this);
//        }
//    }

    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        MapsInitializer.initialize(getContext());
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        googleMap.addMarker(new MarkerOptions().position(new LatLng(48.689247, -74.044582)).title("Statue of Liberty").snippet("I went there already!"));
        CameraPosition Liberty = CameraPosition.builder().target(new LatLng(48.689247, -74.044582)).zoom(16).bearing(8).tilt(45).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Liberty));

//        LatLng sydney = new LatLng(-33.852, 151.211);
//        googleMap.addMarker(new MarkerOptions().position(sydney)
//                .title("Marker in Sydney"));
//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}


/*public class Tab2 extends android.support.v4.app.Fragment {
    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View inflatedView = inflater.inflate(R.layout.tab2, container, false);

        Button button_center = (Button) inflatedView.findViewById(R.id.button_center);

        button_center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getContext(),"Click Me", Toast.LENGTH_LONG);
                toast.show();
                SimpleOrientationActivity myOrientationEventListener = new SimpleOrientationActivity();

            }
        });
        return inflatedView;

        //onSaveInstanceState();
    }

}*/

   /* private void onSaveInstanceState(Bundle outState) {
        outState.putString();
        super.onSaveInstanceState(outState);
    }
    onRestoreInstance
    }*/

//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        AutoCompleteTextView actv = (AutoCompleteTextView) View.findViewById(R.id.autoCompleteTextView1);
//        String[] countries = getResources().getStringArray(R.array.list_of_countries);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>
//                (this,android.R.layout.simple_list_item_1,countries);
//        actv.setAdapter(adapter);
//    }