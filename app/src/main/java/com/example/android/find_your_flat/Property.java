package com.example.android.find_your_flat;

/**
 * Created by andronov on 21-May-17.
 */

public class Property {
    String name;
    String type;
    String size;
    String address;
    int item_img;

    public Property(String name, String type, String size, String address, int item_img){
        this.name=name;
        this.type=type;
        this.size=size;
        this.address=address;
        this.item_img=item_img;
    }

    public String getName() {
        return name;
    }

   public String getType() {
       return type;
   }


    public String getSize() {
        return size;
    }

    public String getAddress() {
        return address;
    }

    public int getItem_img() {
        return item_img;
    }
}
