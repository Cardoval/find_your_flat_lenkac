package com.example.android.find_your_flat;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class Select_num_rooms extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_num_rooms);


        final TextView size_1_1 = (TextView) findViewById(R.id.txt_1_1);
        size_1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDrawable(size_1_1);
            }
        });

        final TextView size_1_kk = (TextView) findViewById(R.id.txt_1_kk);
        size_1_kk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDrawable(size_1_kk);
            }
        });
        final TextView size_2_1 = (TextView) findViewById(R.id.txt_2_1);
        size_2_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDrawable(size_2_1);
            }
        });

        final TextView size_2_kk = (TextView) findViewById(R.id.txt_2_kk);
        size_2_kk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDrawable(size_2_kk);
            }
        });

        final TextView size_3_1 = (TextView) findViewById(R.id.txt_3_1);
        size_3_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDrawable(size_3_1);
            }
        });

        final TextView size_3_kk = (TextView) findViewById(R.id.txt_3_kk);
        size_3_kk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDrawable(size_3_kk);
            }
        });

        final TextView size_4_1 = (TextView) findViewById(R.id.txt_4_1);
        size_4_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDrawable(size_4_1);
            }
        });
        final TextView size_4_kk = (TextView) findViewById(R.id.txt_4_kk);
        size_4_kk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDrawable(size_4_kk);
            }
        });

    }



    public void changeDrawable(TextView v) {
        Drawable backgroundStatus = v.getBackground();
        //Log.i("Angelina debugging", backgroundStatus.getCurrent().toString());
        if (backgroundStatus == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                v.setBackgroundResource(R.drawable.rounded_corners);
            }
        } else {
            v.setBackgroundResource(0);
        }
    }
}