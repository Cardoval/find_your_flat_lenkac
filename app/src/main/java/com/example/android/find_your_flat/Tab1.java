package com.example.android.find_your_flat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.renderscript.Sampler;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.util.BatchingListUpdateCallback;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterViewFlipper;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewAnimator;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import static android.R.attr.animation;
import static android.R.attr.animationDuration;
import static android.R.attr.checked;
import static android.R.attr.color;
import static android.R.attr.colorAccent;
import static android.R.attr.colorPrimary;
import static android.R.attr.content;
import static android.R.attr.duration;
import static android.R.attr.isDefault;
import static android.R.attr.onClick;
import static android.R.attr.slideEdge;
import static android.R.attr.stateListAnimator;
import static android.R.id.primary;
import static android.R.id.toggle;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static android.view.View.INVISIBLE;
import static com.example.android.find_your_flat.R.drawable.flat;
import static com.example.android.find_your_flat.R.drawable.house;
import static com.example.android.find_your_flat.R.drawable.house1;
import static com.example.android.find_your_flat.R.id.btnFlat;
import static com.example.android.find_your_flat.R.id.btnHouse;
import static com.example.android.find_your_flat.R.id.default_activity_button;
import static com.example.android.find_your_flat.R.id.imgFlat;
import static com.example.android.find_your_flat.R.id.imgHouse1;


/**
 * Created by andronov on 21-May-17.
 */

public class Tab1 extends android.support.v4.app.Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View inflatedView = inflater.inflate(R.layout.tab1, container, false);

        final ImageView flat = (ImageView) inflatedView.findViewById(R.id.imgFlat);
        final ImageView house1 = (ImageView) inflatedView.findViewById(R.id.imgHouse1);
        final ViewAnimator animator = (ViewAnimator) inflatedView.findViewById(R.id.animator);

        //initialization of buttons and settting two buttons (Flat and Buy) in default position as "checked"
        final ToggleButton Flat = (ToggleButton) inflatedView.findViewById(R.id.btnFlat);
        Flat.setChecked(true);

        final ToggleButton House = (ToggleButton) inflatedView.findViewById(R.id.btnHouse);
        House.setChecked(false);

        final ToggleButton Buy = (ToggleButton) inflatedView.findViewById(R.id.btnBuy);
        Buy.setChecked(true);

        final ToggleButton Rent = (ToggleButton) inflatedView.findViewById(R.id.btnRent);
        Rent.setChecked(false);

                flat.animate().alpha(0f).setDuration(2000);
        //flat.setVisibility(INVISIBLE);
        //house1.setVisibility(View.VISIBLE);
                house1.animate().alpha(1f).setDuration(2000);







        //creating method OnCheckedChanged - if one button is checked, the second one is unchecked.
        Flat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    House.setChecked(false);

                } else {

                    House.setChecked(true);
                }
            }


        });
        House.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Flat.setChecked(false);
                }
                else {
                    Flat.setChecked(true);
                }
            }
        });
        Buy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                    Rent.setChecked(false);
                }
                else {
                    Rent.setChecked(true);
                }
            }
        });
        Rent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Buy.setChecked(false);
                }
                else {
                    Buy.setChecked(true);
                }
            }
        });


        return inflatedView;


    }









}
