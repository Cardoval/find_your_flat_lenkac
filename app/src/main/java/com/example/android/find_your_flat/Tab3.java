package com.example.android.find_your_flat;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

/**
 * Created by andronov on 21-May-17.
 */

public class Tab3 extends android.support.v4.app.Fragment  {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View inflatedView = inflater.inflate(R.layout.tab3, container, false);


        //initialize seekbar and textfield
        SeekBar seekBar = (SeekBar) inflatedView.findViewById(R.id.seekBar3);
        final TextView txt2 = (TextView) inflatedView.findViewById(R.id.txt2);

        seekBar.setMax(10000000);


        txt2.setText(seekBar.getProgress()+"");

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress=progresValue;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                txt2.setText(progress+"");
            }


        });









        return inflatedView;
    }


}






