package com.example.android.find_your_flat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by andronov on 21-May-17.
 */

public class Tab4 extends android.support.v4.app.Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedview = inflater.inflate(R.layout.tab4, container, false);

        Property properties_data[] = new Property[]
                {
                        new Property("Riverview", "office", "100m2", "Svornosti 5", R.drawable.riverview),
                        new Property("Podolske schody", "flats", "40m2", "Sinkulova 25", R.drawable.podolske_schody)

                };

        ResultListAdapter adapter = new ResultListAdapter(getContext(),R.layout.row_item, properties_data);
        ListView listView1 = (ListView) inflatedview.findViewById(R.id.listView1);
        listView1.setAdapter(adapter);

        return inflatedview;
    }
}
